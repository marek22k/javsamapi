package samapi;

import java.io.IOException;
import java.util.HashMap;
import java.io.Closeable;

/**
 * Simple SAM API
 * @author Marek Küthe
 */
public class samapi implements Closeable, AutoCloseable {
	
	final static String defaultSamHost = "127.0.0.1";
	final static int    defaultSamPort = 7656;
	
	protected tcpclient samcli;
	protected String samversion;
	
	/**
	 * Returns the SAM version specified by the SAM server.
	 * @return The SAM version.
	 */
	public String version() {
		return this.samversion;
	}
	
	/**
	 * Initializes a simple SAM API.
	 * @throws RuntimeException If an error occurs when initializing the connection of the SAM server, the method throws a RuntimeException.
	 * @throws IOException
	 */
	public samapi() throws RuntimeException, IOException {
		initialize(null, null, null, null);
	}
	
	/**
	 * Initializes a simple SAM API.
	 * @param username If the SAM API is protected by authentication, the username can be entered here.
	 * @param password If the SAM API is protected by authentication, the password can be entered here.
	 * @throws RuntimeException If an error occurs when initializing the connection of the SAM server, the method throws a RuntimeException.
	 * @throws IOException
	 */
	public samapi(String username, String password) throws RuntimeException, IOException {
		initialize(null, null, username, password);
	}
	
	/**
	 * Initializes a simple SAM API.
	 * @param host Specifies the host of the SAM server.
	 * @param port Specifies the port of the SAM server.
	 * @throws RuntimeException If an error occurs when initializing the connection of the SAM server, the method throws a RuntimeException.
	 * @throws IOException
	 */
	public samapi(String host, Integer port) throws RuntimeException, IOException {
		initialize(host, port, null, null);
	}
	
	/**
	 * Initializes a simple SAM API.
	 * @param host Specifies the host of the SAM server.
	 * @param port Specifies the port of the SAM server.
	 * @param username If the SAM API is protected by authentication, the username can be entered here.
	 * @param password If the SAM API is protected by authentication, the password can be entered here.
	 * @throws RuntimeException If an error occurs when initializing the connection of the SAM server, the method throws a RuntimeException.
	 * @throws IOException
	 */
	public samapi(String host, Integer port, String username, String password) throws RuntimeException, IOException {
		initialize(host, port, username, password);
	}
	
	protected void initialize(String host, Integer port, String username, String password) throws RuntimeException, IOException {
		if(host == null)
			host = samapi.defaultSamHost;
		if(port == null)
			port = samapi.defaultSamPort;
		
		samcli = new tcpclient(host, port);
		HashMap<String, String> arguments = new HashMap<String, String>();
		if(username != null)
			arguments.put("user", username);
		if(password != null)
			arguments.put("password", password);
		sammsg request = new sammsg("HELLO", "VERSION", arguments);
		//samcli.puts(request.getmessage());
		
		sammsg response = this.sendcommand(request); //sammsg.parsesamresp(samcli.gets());
		if(response.getarg("result").compareTo("OK") != 0) {
			throw new RuntimeException("HELLO VERSION RESULT != OK - is *" + response.getmessage() + "*");
		}
		this.samversion = response.getarg("version");
	}
	
	/**
	 * Sends a complete SAM command to the SAM server.
	 * The SAM message is specified with the class sammsg.
	 * @param msg The full SAM message.
	 * @return Specifies a parsed response in the form of class sammsg.
	 * @throws IOException
	 */
	public sammsg sendcommand(sammsg msg) throws IOException {
		samcli.puts(msg.getmessage());
		String resp = samcli.gets();
		if(resp == null)
			return null;
		return sammsg.parsesamresp(resp);
	}
	
	/**
	 * Sends a raw command that is not specified with the class sammsg to the SAM server.
	 * @param msg The raw command as a string.
	 * @return Returns the loosely non-parsed response from the SAM server.
	 * @throws IOException
	 */
	public String sendrowcommand(String msg) throws IOException {
		samcli.puts(msg);
		String resp = samcli.gets();
		if(resp == null)
			return null;
		return resp;
	}
	
	/**
	 * Closes the connection to the SAM server.
	 * @throws IOException
	 */
	public void close() throws IOException {
		sammsg request = new sammsg("QUIT");
		samcli.puts(request.getmessage());
		samcli.close();
	}
	
}
