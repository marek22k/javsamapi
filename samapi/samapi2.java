package samapi;

import java.io.IOException;
import java.util.HashMap;
import java.io.Closeable;

/**
 * Extended SAM API
 * 
 * SAM API documentation on the I2P website:
 * * v1: https://geti2p.net/en/docs/api/sam
 * * v2: https://geti2p.net/en/docs/api/samv2
 * * v3: https://geti2p.net/en/docs/api/samv3
 * @author Marek Küthe
 */
public class samapi2 extends samapi implements Closeable, AutoCloseable {

	public static enum SESSION_STYLE {
		STREAM,
		DATAGRAM,
		RAW,
		PRIMARY,
		MASTER
	}
	
	public static enum SIGNATURE_TYPE {
		DSA_SHA1,
		ECDSA_SHA256_P256,
		ECDSA_SHA384_P384,
		ECDSA_SHA512_P521,
		RSA_SHA256_2048,
		RSA_SHA384_3072,
		RSA_SHA512_4096,
		EdDSA_SHA512_Ed25519,
		EdDSA_SHA512_Ed25519ph,
		RedDSA_SHA512_Ed25519
	}
	
	protected Thread preplayer = null;
	
	/**
	 * By default, the class samapi2 does not respond to ping requests
	 * from the SAM server, which means that the connection can be
	 * terminated if a socket is not used for a longer period of time.
	 * This can be annoying with a "Stream Forward" command.
	 * When startPingReplayer is called, a thread is started in the
	 * background that reacts to ping requests from the SAM server.
	 * The thread is closed again by the function close. This function
	 * should therefore only be used if this connection to the SAM server
	 * is no longer required (e.g. with stream forward).
	 */
	public void startPingReplayer() {
		preplayer = new Thread(new PingReplayer());
		preplayer.start();
	}
	
	@Override
	public void close() throws IOException {
		preplayer.interrupt();
		super.close();
	}
	
	protected class PingReplayer implements Runnable {
		
		@Override
		public void run() {
			try {
				String buffer = null;
				while(true) {
					buffer = samcli.gets();
					samcli.puts("PONG " + buffer.substring(buffer.indexOf(' ') + 1));
				}
			} catch(IOException e) {
				return;
			}
		}
		
	}
	
	/**
	 * Initializes the extended SAM API. The arguments are the same as for the simple SAM API.
	 * @throws RuntimeException
	 * @throws IOException
	 */
	public samapi2() throws RuntimeException, IOException {
		super(null, null, null, null);
	}
	
	/**
	 * Initializes the extended SAM API. The arguments are the same as for the simple SAM API.
	 * @param username
	 * @param password
	 * @throws RuntimeException
	 * @throws IOException
	 */
	public samapi2(String username, String password) throws RuntimeException, IOException {
		super(null, null, username, password);
	}
	
	/**
	 * Initializes the extended SAM API. The arguments are the same as for the simple SAM API.
	 * @param host
	 * @param port
	 * @throws RuntimeException
	 * @throws IOException
	 */
	public samapi2(String host, Integer port) throws RuntimeException, IOException {
		super(host, port, null, null);
	}
	
	/**
	 * Initializes the extended SAM API. The arguments are the same as for the simple SAM API.
	 * @param host
	 * @param port
	 * @param username
	 * @param password
	 * @throws RuntimeException
	 * @throws IOException
	 */
	public samapi2(String host, Integer port, String username, String password) throws RuntimeException, IOException {
		super(host, port, username, password);
	}
	
	/**
	 * Resolves the address of an .i2p site (eepsite) into a Base64.
	 * @param name The .i2p site (eepsite).
	 * @return Returns the resolved Base64.
	 * @throws IOException
	 * @throws RuntimeException If an error occurs while resolving the Base64, the method throws a RuntimeException.
	 */
	public String lookup(String name) throws IOException, RuntimeException {
		HashMap<String, String> arguments = new HashMap<String, String>();
		arguments.put("NAME", name);
		sammsg request = new sammsg("NAMING", "LOOKUP", arguments);
		sammsg resp = super.sendcommand(request);
		if(resp.getarg("result").compareTo("OK") != 0) {
			throw new RuntimeException(resp.getarg("result"));
		}
		return resp.getarg("VALUE");
	}
	
	/**
	 * Sends a ping to the SAM server so that the connection is not interrupted.
	 * To check whether the ping was successful, the current time is sent in seconds.
	 * If the time comes back as it was sent, true is returned, otherwise false.
	 * @return Returns true if the ping was sent successfully, otherwise false.
	 * @throws IOException
	 */
	public boolean keepawakeping() throws IOException {
		return ping(Long.toString(System.currentTimeMillis()));
	}
	
	/**
	 * Sends a ping to the SAM server.
	 * @return Returns true if a response (PONG) is received, otherwise false.
	 * @throws IOException
	 */
	public boolean ping() throws IOException {
		if(super.sendrowcommand("PING").trim().compareTo("PONG") == 0)
			return true;
		else
			return false;
	}
	
	/**
	 * The text that should be sent for review.
	 * @param arb The text that should be sent for review. If the server does not return the same text with the response (PONG), the ping is considered to have failed.
	 * @return Returns true if the ping was successful, otherwise false.
	 * @throws IOException
	 */
	public boolean ping(String arb) throws IOException {
		sammsg request = new sammsg("PING", arb);
		String resp = super.sendrowcommand(request.getmessage()).trim();
		if(resp.substring(resp.indexOf(' ') + 1).compareTo(arb) == 0)
			return true;
		else
			return false;
	}

	@SuppressWarnings("incomplete-switch")
	protected String createmastersession(SESSION_STYLE sessionstyle, String id, String dest, HashMap<String, String> options) throws IOException {
		HashMap<String, String> arguments = new HashMap<String, String>();
		
		if(options != null)
			arguments.putAll(options);

		switch(sessionstyle) {
		case MASTER:
			arguments.put("style", "MASTER");
			break;
		case PRIMARY:
			arguments.put("style", "primary");
			break;
		}
		
		arguments.put("id", id);
		
		if(dest == null)
			arguments.put("destination", "TRANSIENT");
		else
			arguments.put("destination", dest);
		
		sammsg request = new sammsg("SESSION", "CREATE", arguments);
		sammsg resp = super.sendcommand(request);
		
		if(resp.getarg("result").compareTo("OK") != 0) {
			throw new RuntimeException(resp.getarg("result"));
		}
		
		return resp.getarg("destination");
	}
	
	/**
	 * Adds a sub session to a master session.
	 * @param sessionstyle Specifies which session style to add.
	 * @param id Specifies the ID of the session.
	 * @return Returns the message from the SAM server.
	 * @throws IOException
	 */
	public String addsession(SESSION_STYLE sessionstyle, String id) throws IOException {
		return addsession(sessionstyle, id, null, null, null, null, null, null, null, null, null);
	}
	
	/**
	 * Adds a sub session to a master session.
	 * @param sessionstyle Specifies which session style to add.
	 * @param id Specifies the ID of the session.
	 * @param options The arguments to be added to the SAM message sent to the SAM server.
	 * @return Returns the message from the SAM server.
	 * @throws IOException
	 */
	public String addsession(SESSION_STYLE sessionstyle, String id, HashMap<String, String> options) throws IOException {
		return addsession(sessionstyle, id, null, null, null, null, null, null, null, null, options);
	}
	
	/**
	 * Adds a sub session to a master session.
	 * For a documentation of the individual parameters, one can see the SAM API documentation on the I2P website.
	 * @param sessionstyle Specifies which session style to add.
	 * @param id Specifies the ID of the session.
	 * @param port
	 * @param host
	 * @param from_port
	 * @param to_port
	 * @param protocol
	 * @param listen_port
	 * @param listen_protocol
	 * @param header
	 * @param options The arguments to be added to the SAM message sent to the SAM server.
	 * @return Returns the message from the SAM server.
	 * @throws IOException
	 */
	@SuppressWarnings("incomplete-switch")
	public String addsession(SESSION_STYLE sessionstyle, String id, Integer port, String host,
			Integer from_port, Integer to_port, Integer protocol, Integer listen_port,
			Integer listen_protocol, Boolean header, HashMap<String, String> options) throws IOException {
		HashMap<String, String> arguments = new HashMap<String, String>();
		
		if(options != null) {
			arguments.putAll(options);
		}
		
		switch(sessionstyle) {
		case STREAM:
			arguments.put("style", "STREAM");
		case DATAGRAM:
			arguments.put("style", "DATAGRAM");
		case RAW:
			arguments.put("style", "RAW");
		}
		
		arguments.put("id", id);
		
		if(port != null)
			arguments.put("port", port.toString());
		
		if(host != null)
			arguments.put("host", host);
		
		if(from_port != null)
			arguments.put("from_port", from_port.toString());
		
		if(to_port != null)
			arguments.put("to_port", to_port.toString());
		
		if(protocol != null)
			arguments.put("protocol", protocol.toString());
		
		if(listen_port != null)
			arguments.put("listen_port", listen_port.toString());
		
		if(listen_protocol != null)
			arguments.put("listen_protocol", listen_protocol.toString());
		
		if(header != null)
			arguments.put("header", header.toString());
		
		sammsg request = new sammsg("session", "add", arguments);
		sammsg resp = super.sendcommand(request);
		
		if(resp.getarg("result").compareTo("OK") != 0)
			throw new RuntimeException(resp.getarg("result") + " - " + resp.getarg("message"));
		
		return resp.getarg("message");
	}
	
	/**
	 * Deletes a sub session
	 * @param id The ID of the sub session to be removed.
	 * @return Returns the message from the SAM server.
	 * @throws IOException
	 */
	public String removesession(String id) throws IOException {
		HashMap<String, String> arguments = new HashMap<String, String>();
		arguments.put("id", id);
		sammsg request = new sammsg("session", "remove", arguments);
		sammsg resp = super.sendcommand(request);
		
		if(resp.getarg("result").compareTo("OK") != 0)
			throw new RuntimeException(resp.getarg("result") + " - " + resp.getarg("message"));
		
		return resp.getarg("message");
	}
	
	/**
	 * Creates a session.
	 * For a documentation of the individual parameters, one can see the SAM API documentation on the I2P website.
	 * @param sessionstyle Specifies which session style to add.
	 * @param id Specifies the ID of the session.
	 * @param dest
	 * @param options The arguments to be added to the SAM message sent to the SAM server.
	 * @return Returns the destination to which the session has been bound.
	 * @throws IOException
	 */
	public String createsession(SESSION_STYLE sessionstyle, String id, String dest, HashMap<String, String> options) throws IOException {
		if(sessionstyle == SESSION_STYLE.MASTER || sessionstyle == SESSION_STYLE.PRIMARY) {
			return createmastersession(sessionstyle, id, dest, options);
		} else {
			return createsession(sessionstyle, id, dest, null, null, null, null, null, null, null, options);
		}
	}

	/**
	 * Creates a session.
	 * For a documentation of the individual parameters, one can see the SAM API documentation on the I2P website.
	 * @param sessionstyle Specifies which session style to add.
	 * @param id Specifies the ID of the session.
	 * @param dest
	 * @return Returns the destination to which the session has been bound.
	 * @throws IOException
	 */
	public String createsession(SESSION_STYLE sessionstyle, String id, String dest) throws IOException {
		if(sessionstyle == SESSION_STYLE.MASTER || sessionstyle == SESSION_STYLE.PRIMARY) {
			return createmastersession(sessionstyle, id, dest, null);
		} else {
			return createsession(sessionstyle, id, dest, null, null, null, null, null, null, null, null);
		}
	}
	
	/**
	 * Creates a session.
	 * For a documentation of the individual parameters, one can see the SAM API documentation on the I2P website.
	 * @param sessionstyle Specifies which session style to add.
	 * @param id Specifies the ID of the session.
	 * @param dest
	 * @param sigtype
	 * @param options The arguments to be added to the SAM message sent to the SAM server.
	 * @return Returns the destination to which the session has been bound.
	 * @throws IOException
	 */
	public String createsession(SESSION_STYLE sessionstyle, String id, String dest,
								SIGNATURE_TYPE sigtype, HashMap<String, String> options) throws IOException {
		return createsession(sessionstyle, id, dest, sigtype, null, null, null, null, null, null, options);
	}

	/**
	 * Creates a session.
	 * For a documentation of the individual parameters, one can see the SAM API documentation on the I2P website.
	 * @param sessionstyle Specifies which session style to add.
	 * @param id Specifies the ID of the session.
	 * @param dest
	 * @param sigtype
	 * @return Returns the destination to which the session has been bound.
	 * @throws IOException
	 */
	public String createsession(SESSION_STYLE sessionstyle, String id, String dest,
								SIGNATURE_TYPE sigtype) throws IOException {
		return createsession(sessionstyle, id, dest, sigtype, null, null, null, null, null, null, null);
	}
	
	/**
	 * Creates a session.
	 * For a documentation of the individual parameters, one can see the SAM API documentation on the I2P website.
	 * @param sessionstyle Specifies which session style to add.
	 * @param id Specifies the ID of the session.
	 * @param dest
	 * @param sigtype
	 * @param port
	 * @param host
	 * @param from_port
	 * @param to_port
	 * @param protocol
	 * @param header
	 * @param options The arguments to be added to the SAM message sent to the SAM server.
	 * @return Returns the destination to which the session has been bound.
	 * @throws IOException
	 */
	@SuppressWarnings("incomplete-switch")
	public String createsession(SESSION_STYLE sessionstyle, String id, String dest,
								SIGNATURE_TYPE sigtype, Integer port, String host,
								Integer from_port, Integer to_port, Integer protocol,
								Boolean header, HashMap<String, String> options) throws IOException {
		HashMap<String, String> arguments = new HashMap<String, String>();
		switch(sessionstyle) {
		case RAW:
			arguments.put("STYLE", "RAW");
			break;
		case DATAGRAM:
			arguments.put("STYLE", "DATAGRAM");
			break;
		case STREAM:
			arguments.put("STYLE", "STREAM");
			break;
		}
		
		arguments.put("ID", id);
		
		if(dest == null)
			arguments.put("DESTINATION", "TRANSIENT");
		else
			arguments.put("DESTINATION", dest);
		
		if(sigtype != null)
			switch(sigtype) {
			case DSA_SHA1:
				arguments.put("SIGNATURE_TYPE", "DSA_SHA1");
				break;
			case ECDSA_SHA256_P256:
				arguments.put("SIGNATURE_TYPE", "ECDSA_SHA256_P256");
				break;
			case ECDSA_SHA384_P384:
				arguments.put("SIGNATURE_TYPE", "ECDSA_SHA384_P384");
				break;
			case ECDSA_SHA512_P521:
				arguments.put("SIGNATURE_TYPE", "ECDSA_SHA512_P521");
				break;
			case EdDSA_SHA512_Ed25519:
				arguments.put("SIGNATURE_TYPE", "EdDSA_SHA512_Ed25519");
				break;
			case EdDSA_SHA512_Ed25519ph:
				arguments.put("SIGNATURE_TYPE", "EdDSA_SHA512_Ed25519ph");
				break;
			case RedDSA_SHA512_Ed25519:
				arguments.put("SIGNATURE_TYPE", "RedDSA_SHA512_Ed25519");
				break;
			case RSA_SHA256_2048:
				arguments.put("SIGNATURE_TYPE", "RSA_SHA256_2048");
				break;
			case RSA_SHA384_3072:
				arguments.put("SIGNATURE_TYPE", "RSA_SHA384_3072");
				break;
			case RSA_SHA512_4096:
				arguments.put("SIGNATURE_TYPE", "RSA_SHA512_4096");
				break;
			}
		
		if(port != null)
			arguments.put("PORT", port.toString());
		
		if(host != null)
			arguments.put("HOST", host);
		
		if(from_port != null)
			arguments.put("FROM_PORT", from_port.toString());
		
		if(to_port != null)
			arguments.put("TO_PORT", to_port.toString());
		
		if(protocol != null)
			arguments.put("PROTOCOL", protocol.toString());
		
		if(header != null)
			arguments.put("HEADER", header.toString());
		
		if(options != null) {
			arguments.putAll(options);
		}
		
		sammsg request = new sammsg("SESSION", "CREATE", arguments);
		sammsg resp = super.sendcommand(request);
		
		if(resp.getarg("RESULT").compareTo("OK") != 0) {
			throw new RuntimeException(resp.getarg("result"));
		}
		
		return resp.getarg("DESTINATION");
	}
	
	/**
	 * Connects to a destination.
	 * For a documentation of the individual parameters, one can see the SAM API documentation on the I2P website.
	 * @param id
	 * @param dest
	 * @return Returns a tcpclient, which can then be used to connect to the destination.
	 * @throws RuntimeException
	 * @throws IOException
	 */
	public tcpclient streamconnect(String id, String dest) throws RuntimeException, IOException {
		return streamconnect(id, dest, null, null, null);
	}
	
	/**
	 * Connects to a destination.
	 * For a documentation of the individual parameters, one can see the SAM API documentation on the I2P website.
	 * @param id
	 * @param dest
	 * @param silent
	 * @param from_port
	 * @param to_port
	 * @return Returns a tcpclient, which can then be used to connect to the destination.
	 * @throws RuntimeException
	 * @throws IOException
	 */
	public tcpclient streamconnect(String id, String dest, Boolean silent,
									Integer from_port, Integer to_port) throws RuntimeException, IOException {
		HashMap<String, String> arguments = new HashMap<String, String>();
		arguments.put("ID", id);
		arguments.put("DESTINATION", dest);
		
		if(silent != null)
			arguments.put("SILENT", silent.toString());
		
		if(from_port != null)
			arguments.put("FROM_PORT", silent.toString());
		
		if(to_port != null)
			arguments.put("TO_PORT", silent.toString());
		
		sammsg request = new sammsg("STREAM", "CONNECT", arguments);
		sammsg resp = super.sendcommand(request);
		
		if(resp.getarg("result").compareTo("OK") != 0) {
			throw new RuntimeException(resp.getarg("result") + " - " + resp.getarg("message"));
		}
		
		return super.samcli;
	}
	
	/**
	 * Activates the authentication.
	 * @throws IOException
	 * @throws RuntimeException
	 */
	public void authenable() throws IOException, RuntimeException {
		sammsg request = new sammsg("auth", "enable");
		sammsg resp = super.sendcommand(request);

		if(resp.getarg("result").compareTo("OK") != 0) {
			throw new RuntimeException(resp.getarg("result") + " - " + resp.getarg("message"));
		}
	}
	
	/**
	 * Deactivates the authentication.
	 * @throws IOException
	 * @throws RuntimeException
	 */
	public void authdisable() throws IOException, RuntimeException {
		sammsg request = new sammsg("auth", "disable");
		sammsg resp = super.sendcommand(request);

		if(resp.getarg("result").compareTo("OK") != 0) {
			throw new RuntimeException(resp.getarg("result") + " - " + resp.getarg("message"));
		}
	}
	
	/**
	 * Added a user who can use the SAM API.
	 * @param username The username of the new user.
	 * @param password The password of the new user.
	 * @throws IOException
	 * @throws RuntimeException
	 */
	public void authadduser(String username, String password) throws IOException, RuntimeException {
		HashMap<String, String> arguments = new HashMap<String, String>();
		arguments.put("user", "\"" + username + "\"");
		arguments.put("password", "\"" + password + "\"");
		sammsg request = new sammsg("auth", "add", arguments);
		sammsg resp = super.sendcommand(request);
		
		if(resp.getarg("result").compareTo("OK") != 0) {
			throw new RuntimeException(resp.getarg("result") + " - " + resp.getarg("message"));
		}
	}
	
	/**
	 * Deletes a user who is allowed to connect to the SAM API.
	 * @param username The username of the user to be deleted.
	 * @throws IOException
	 * @throws RuntimeException
	 */
	public void authremoveuser(String username) throws IOException, RuntimeException {
		HashMap<String, String> arguments = new HashMap<String, String>();
		arguments.put("user", "\"" + username + "\"");
		sammsg request = new sammsg("auth", "remove", arguments);
		sammsg resp = super.sendcommand(request);
		
		if(resp.getarg("result").compareTo("OK") != 0) {
			throw new RuntimeException(resp.getarg("result") + " - " + resp.getarg("message"));
		}
	}
	
	/**
	 * Requests help from the SAM API.
	 * Note: This function is not supported by all SAM servers.
	 * @return The help (answer) of the server.
	 * @throws IOException
	 */
	public String help() throws IOException {
		sammsg request = new sammsg("help");
		return super.sendrowcommand(request.getmessage());
	}
	
	/**
	 * Requests help via a specific command from the SAM server.
	 * Note: This function is not supported by all SAM servers.
	 * @param cmd The command about which help should be requested.
	 * @return The help (answer) of the server.
	 * @throws IOException
	 */
	public String help(String cmd) throws IOException {
		sammsg request = new sammsg("help", cmd);
		return super.sendrowcommand(request.getmessage());
	}
	
	/**
	 * Generates a new destination and returns the public and private keys in an array.
	 * @return Array with the public and private keys. The first entry contains the public key. The second entry contains the private key.
	 * @throws IOException
	 */
	public String[] destgenerate() throws IOException {
		return destgenerate(null);
	}
	
	/**
	 * Generates a new destination and returns the public and private keys in an array.
	 * @param sigtype Specifies which signature the destination should use.
	 * @return Array with the public and private keys. The first entry contains the public key. The second entry contains the private key.
	 * @throws IOException
	 */
	// return: pub, priv
	public String[] destgenerate(SIGNATURE_TYPE sigtype) throws IOException {
		HashMap<String, String> arguments = new HashMap<String, String>();
		
		if(sigtype != null)
			switch(sigtype) {
			case DSA_SHA1:
				arguments.put("SIGNATURE_TYPE", "DSA_SHA1");
				break;
			case ECDSA_SHA256_P256:
				arguments.put("SIGNATURE_TYPE", "ECDSA_SHA256_P256");
				break;
			case ECDSA_SHA384_P384:
				arguments.put("SIGNATURE_TYPE", "ECDSA_SHA384_P384");
				break;
			case ECDSA_SHA512_P521:
				arguments.put("SIGNATURE_TYPE", "ECDSA_SHA512_P521");
				break;
			case EdDSA_SHA512_Ed25519:
				arguments.put("SIGNATURE_TYPE", "EdDSA_SHA512_Ed25519");
				break;
			case EdDSA_SHA512_Ed25519ph:
				arguments.put("SIGNATURE_TYPE", "EdDSA_SHA512_Ed25519ph");
				break;
			case RedDSA_SHA512_Ed25519:
				arguments.put("SIGNATURE_TYPE", "RedDSA_SHA512_Ed25519");
				break;
			case RSA_SHA256_2048:
				arguments.put("SIGNATURE_TYPE", "RSA_SHA256_2048");
				break;
			case RSA_SHA384_3072:
				arguments.put("SIGNATURE_TYPE", "RSA_SHA384_3072");
				break;
			case RSA_SHA512_4096:
				arguments.put("SIGNATURE_TYPE", "RSA_SHA512_4096");
				break;
			}
		
		sammsg request = new sammsg("DEST", "GENERATE", arguments);
		sammsg resp = super.sendcommand(request);
		
		String[] retur = {resp.getarg("pub"), resp.getarg("priv")};
		
		return retur;
	}
	
	/**
	 * Accepts an incoming connection from a stream session.
	 * @param id
	 * @return Returns four objects in an array.
	 * 		The first entry is the TCP client (of the class tcpclient), with which you can then communicate with the remote station.
	 * 		The second entry is the Base64 (of the class String) of the remote station.
	 *		The third entry specifies the port (of class Integer) from which sending is being made.
	 * 		The fourth entry indicates on which port (of class Integer) is received.
	 * @throws IOException
	 */
	public Object[] streamaccept(String id) throws IOException {
		return streamaccept(id, null);
	}
	
	/**
	 * Accepts an incoming connection from a stream session.
	 * @param id
	 * @param silent If silent=true is specified, no info line is sent. Therefore only the TCP client is returned as the first and only entry.
	 * @return Returns four objects in an array.
	 * 		The first entry is the TCP client (of the class tcpclient), with which you can then communicate with the remote station.
	 * 		The second entry is the Base64 (of the class String) of the remote station.
	 *		The third entry specifies the port (of class Integer) from which sending is being made.
	 * 		The fourth entry indicates on which port (of class Integer) is received.
	 * @throws IOException
	 */
	// return: tcpclient, base64, fromport, toport
	public Object[] streamaccept(String id, Boolean silent) throws IOException {
		HashMap<String, String> arguments = new HashMap<String, String>();
		arguments.put("ID", id);
		
		if(silent != null)
			arguments.put("SILENT", silent.toString());
		
		sammsg request = new sammsg("STREAM", "ACCEPT", arguments);
		sammsg resp = super.sendcommand(request);
		
		if(resp.getarg("result").compareTo("OK") != 0) {
			throw new RuntimeException(resp.getarg("result") + " - " + resp.getarg("message"));
		}
		
		if(silent != true) {
			String metadata = this.samcli.gets();
			int tofrom = metadata.indexOf(' ');
			int toto = metadata.indexOf(' ', tofrom + 1);
			String fromport = metadata.substring(tofrom + 1, toto).trim();
			String toport = metadata.substring(toto + 1).trim();
			
			Object[] mretur = {
					this.samcli,
					metadata.substring(0, tofrom).trim(),
					Integer.parseInt(fromport.substring(fromport.indexOf('=') + 1)),
					Integer.parseInt(toport.substring(toport.indexOf('=') + 1))
				};
			return mretur;
		} else {
			Object[] mretur = {
					this.samcli
			};
			return mretur;
		}
	}
	
	/**
	 * Redirects incoming connections to a server socket.
	 * @param id
	 * @param port The port of the server on which the connections should be forwarded. By default, the host is 127.0.0.1 or localhost.
	 * @throws IOException
	 */
	public void streamforward(String id, Integer port) throws IOException {
		streamforward(id, port, null, null, null);
	}
	
	/**
	 * Redirects incoming connections to a server socket.
	 * For a documentation of the individual parameters, one can see the SAM API documentation on the I2P website.
	 * @param id
	 * @param port The port of the server on which the connections should be forwarded. By default, the host is 127.0.0.1 or localhost.
	 * @param host The host on which the connections should be forwarded.
	 * @param silent
	 * @param ssl
	 * @throws IOException
	 */
	public void streamforward(String id, Integer port, String host, Boolean silent, Boolean ssl) throws IOException {
		HashMap<String, String> arguments = new HashMap<String, String>();
		arguments.put("ID", id);
		arguments.put("PORT", port.toString());
		
		if(host != null)
			arguments.put("HOST", host);
		
		if(silent != null)
			arguments.put("SILENT", silent.toString());
		
		if(ssl != null)
			arguments.put("SSL", silent.toString());
		
		sammsg request = new sammsg("STREAM", "FORWARD", arguments);
		sammsg resp = super.sendcommand(request);
		
		if(resp.getarg("result").compareTo("OK") != 0) {
			throw new RuntimeException(resp.getarg("result") + " - " + resp.getarg("message"));
		}
	}
	
	public boolean isStillAlive() {
		return this.samcli.isOpen();
	}

}
