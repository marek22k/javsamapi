package samapi;

import java.io.IOException;
import java.util.HashMap;

public class StreamSession {

	protected RandomStringGenerator stringgenerator;
	protected String id;
	public samapi2 sam;
	protected String pubkey;
	protected String privkey;
	final static samapi2.SIGNATURE_TYPE defaultSignatureType = samapi2.SIGNATURE_TYPE.EdDSA_SHA512_Ed25519;
	
	protected String username;
	protected String password;
	protected String host;
	protected Integer port;
	
	public class StreamForwarder {
		
		protected samapi2 forwarder;
		
		public void forward(String host, Integer port) throws IOException {
			forwarder = new samapi2(StreamSession.this.host, StreamSession.this.port, StreamSession.this.username, StreamSession.this.password);
			forwarder.streamforward(StreamSession.this.id, port, host, null, null);
			forwarder.startPingReplayer();
		}
		
	}
	
	public class StreamConnector {
		
		protected samapi2 conn;
		
		public tcpclient connect(String base64) throws IOException {
			conn = new samapi2(StreamSession.this.host, StreamSession.this.port, StreamSession.this.username, StreamSession.this.password);
			return conn.streamconnect(StreamSession.this.id, base64);
		}
		
	}  // end class streamconnecter
	
	public class StreamAcceptor {
		
		protected samapi2 acceptor;
		protected tcpclient client;
		protected String base64;
		protected int to_port;
		protected int from_port;
		
		public Object[] accept() throws IOException {
			acceptor = new samapi2(StreamSession.this.host, StreamSession.this.port, StreamSession.this.username, StreamSession.this.password);
			Object[] res = acceptor.streamaccept(StreamSession.this.id);
			this.client = (tcpclient) res[0];
			this.base64 = (String) res[1];
			this.from_port = (Integer) res[2];
			this.to_port = (Integer) res[3];
			return res;
		}
		
		public String requester() {
			return this.base64;
		}
		
		public int to_port() {
			return this.to_port;
		}
		
		public int from_port() {
			return this.from_port;
		}
		
		public tcpclient client() {
			return this.client;
		}
		
	}  // end class StreamAcceptor
	
	public StreamSession() throws RuntimeException, IOException {
		sam = new samapi2();
	}
	
	public StreamSession(String username, String password) throws RuntimeException, IOException {
		sam = new samapi2(username, password);
		this.username = username;
		this.password = password;
	}
	
	public StreamSession(String host, Integer port) throws RuntimeException, IOException {
		sam = new samapi2(host, port);
		this.host = host;
		this.port = port;
	}
	
	public StreamSession(String host, Integer port, String username, String password) throws RuntimeException, IOException {
		sam = new samapi2(host, port, username, password);
		this.host = host;
		this.port = port;
		this.username = username;
		this.password = password;
	}
	
	public void initialize() throws IOException {
		initialize(null, null, null);
	}
	
	public void initialize(String privkey, String pubkey) throws IOException {
		initialize(privkey, pubkey, null);
	}
	
	public void initialize(String privkey, String pubkey, HashMap<String, String> options) throws IOException {
		stringgenerator = new RandomStringGenerator();
		id = stringgenerator.generateRandomString(10);
		
		if(privkey == null && pubkey == null) {
			String[] keys = sam.destgenerate(defaultSignatureType);
			this.pubkey = keys[0];
			this.privkey = keys[1];
		} else if(privkey != null && pubkey != null) {
			this.pubkey = pubkey;
			this.privkey = privkey;
		} else {
			throw new RuntimeException("privkey, pubkey are not both null or fulled");
		}
		
		sam.createsession(samapi2.SESSION_STYLE.STREAM, id, this.privkey, defaultSignatureType, options);
		sam.startPingReplayer();
	}
	
	public String privkey() {
		return this.privkey;
	}
	
	public String pubkey() {
		return this.pubkey;
	}
	
	public String lookup(String name) throws IOException {
		return sam.lookup(name);
	}
	
}