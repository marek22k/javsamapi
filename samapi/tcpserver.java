
package samapi;

import java.io.IOException;
import java.io.Closeable;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.function.Consumer;

public class tcpserver implements Closeable, AutoCloseable {

	Consumer<tcpclient> acceptHandler;
	Consumer<Exception> exceptionHandler;
	ServerSocket serv;
	tcpserver.BackgroundServer bgserv;
	Thread trserv;
	int ac = 0;
	
	public tcpserver(Integer port) throws IOException {
		initialize(port, -1, null);
	}
	
	public tcpserver(Integer port, String host) throws UnknownHostException, IOException {
		initialize(port, -1, InetAddress.getByName(host));
	}
	
	public tcpserver(Integer port, Integer maxrequested, InetAddress addr) throws IOException {
		initialize(port, maxrequested, addr);
	}
	
	private void initialize(Integer port, Integer maxrequested, InetAddress addr) throws IOException {
		serv = new ServerSocket(port, maxrequested, addr);
		bgserv = new BackgroundServer();
		trserv = new Thread(bgserv);
	}
	
	public void accept(Consumer<tcpclient> acceptHandler, Consumer<Exception> exceptionHandler) {
		this.acceptHandler = acceptHandler;
		this.exceptionHandler = exceptionHandler;
		trserv.start();
	}
	
	public void accept_wait() throws InterruptedException {
		trserv.join();
		while(ac != 0);
	}
	
	public void close() throws IOException {
		serv.close();
	}
	
	protected class BackgroundServer implements Runnable {
		
		protected class Connection implements Runnable {
			
			Socket sock;
			tcpclient cli;
			
			public Connection(Socket sock) {
				this.sock = sock;
			}
			
			@Override
			public void run() {
				ac++;
				try {
					cli = new tcpclient(sock);
					acceptHandler.accept(cli);
				} catch(Exception e) {
					exceptionHandler.accept(e);
				}
				ac--;
			}
			
		}
		
		@Override
		public void run() {
			
			while(! serv.isClosed()) {
				try {
					Thread conntr = new Thread(new Connection(serv.accept()));
					conntr.start();
				} catch(Exception e) {
					exceptionHandler.accept(e);
				}
			}
			
		}
		
	}
	
}
