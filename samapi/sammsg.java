package samapi;

import java.util.HashMap;

/**
 * A class for storing SAM messages.
 * @author Marek Küthe
 */
public class sammsg {
	
	public String first;
	public String second;
	public HashMap<String, String> arguments;
	
	/**
	 * Initializes the SAM message with a command.
	 * @param cmd The send command.
	 */
	public sammsg(String cmd) {
		initialize(cmd, null, null);
	}
	
	/**
	 * Initializes the SAM message with a command and arguments.
	 * @param cmd The send command.
	 * @param args The arguments to send.
	 */
	public sammsg(String cmd, HashMap<String, String> args) {
		initialize(cmd, null, args);
	}
	
	/**
	 * Initializes the SAM message with a main command and a subcommand.
	 * @param fircmd The main command.
	 * @param seccmd The subcommand.
	 */
	public sammsg(String fircmd, String seccmd) {
		initialize(fircmd, seccmd, null);
	}
	
	/**
	 * Initializes the SAM message with a main command, a subcommand and arguments.
	 * @param fircmd The main command.
	 * @param seccmd The subcommand.
	 * @param args The arguments to send.
	 */
	public sammsg(String fircmd, String seccmd, HashMap<String, String> args) {
		initialize(fircmd, seccmd, args);
	}
	
	/**
	 * Reads one of the arguments of the SAM message and returns its content.
	 * @param arg The argument to read.
	 * @return The content of the argument read.
	 */
	public String getarg(String arg) {
		String res = arguments.get(arg);
		return (res == null ? arguments.get(arg.toUpperCase()) : res);
	}
	
	/**
	 * Returns the SAM message as a complete SAM command that can be sent to the SAM server.
	 * @return The complete command that can be sent to the SAM server.
	 */
	public String getmessage() {
		String cmd = this.first.toUpperCase();
		if(this.second != null) {
			cmd += " " + this.second.toUpperCase();
		}
		
		for(String key : this.arguments.keySet()) {
			cmd += " " + key + "=" + this.arguments.get(key);
		}
		
		return cmd;
	}
	
	protected void initialize(String fircmd,
			String seccmd,
			HashMap<String, String> args) {
		this.first = fircmd;
		this.second = seccmd;
		this.arguments = ( args == null ? new HashMap<String, String>() : args);
	}
	
	public static sammsg parsesamresp(String resp) {
		int firpos = resp.indexOf(' ');
		String first = resp.substring(0, firpos);
		int secpos = resp.indexOf(' ', firpos + 1);
		String second = resp.substring(firpos+1, secpos);
		HashMap<String, String> args = new HashMap<String, String>();
		while( firpos < resp.length() ) {
			firpos = resp.indexOf(' ', secpos + 1);
			if(firpos == -1) {
				firpos = resp.length();
			}
			String arg = resp.substring(secpos + 1, firpos);
			int spliter = arg.indexOf("=");
			if(arg.charAt(spliter + 1) == '"') {
				firpos = resp.indexOf('"', secpos + spliter + 3) + 1;
				args.put(arg.substring(0, spliter), resp.substring(secpos + spliter + 3, firpos - 1));
			} else {
				args.put(arg.substring(0, spliter), arg.substring(spliter + 1, arg.length()));
			}
			secpos = firpos;
		}
		
		return new sammsg(first, second, args);
	}
	
}
