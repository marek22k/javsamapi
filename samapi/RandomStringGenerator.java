package samapi;

public class RandomStringGenerator {

	final static String standardsigns = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	String signs;
	
	RandomStringGenerator() {
		this.signs = RandomStringGenerator.standardsigns;
	}
	
	RandomStringGenerator(String signs) {
		this.signs = signs;
	}
	
	public String generateRandomString(long length) {
		String res = "";
		
		for(long i = 0; i < length; i++)
		{
			res += signs.charAt((int) Math.floor(Math.random() * signs.length()));
		}
		
		return res;
	}
	
}
