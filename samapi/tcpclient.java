package samapi;

import java.io.DataOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.Closeable;
import java.net.Socket;

public class tcpclient implements Closeable, AutoCloseable {
	
	protected DataOutputStream out;
	protected BufferedReader   in;
	protected Socket		   sock;
	
	protected String 		   host;
	protected int			   port;
	
	/**
	 * Initializes the TCP client
	 * @param host Specifies the host to which the connection is to be established.
	 * @param port Specifies the port to which the connection is to be established.
	 */
	public tcpclient(String host, int port) throws IOException {
		sock = new Socket(host, port);
        out = new DataOutputStream(sock.getOutputStream());
	    in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
	}
	
	public tcpclient(Socket sock) throws IOException {
		this.sock = sock;
		out = new DataOutputStream(sock.getOutputStream());
		in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
	}
	
	public void close() throws IOException {
		this.sock.close();
	}
	
	/**
	 * Sends a series of bytes to the socket.
	 * 
	 * @param str
	 * @throws IOException
	 */
	public void puts(String str) throws IOException {
		puts(str.getBytes());
	}
	
	/**
	 * Decodes a string and sends it to the socket followed by a line break.
	 * @param bytes
	 * @throws IOException
	 */
	public void puts(byte[] bytes) throws IOException {
		out.write(bytes, 0, bytes.length);
		out.write(new byte[] {10}, 0, 1);
	}
	
	/**
	 * Reads a line from the socket. Returns null when no more rows can be read.
	 * @return Returns the line read.
	 * @throws IOException
	 */
	public String gets() throws IOException {
		return in.readLine();
	}
	
	public boolean isOpen() {
		return ! this.sock.isClosed();
	}
	
	public boolean isClose() {
		return this.sock.isClosed();
	}
	
}
