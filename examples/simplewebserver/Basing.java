
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;

public class Basing {
	
    final static HashMap<Integer, Character> Base32Table = new HashMap<Integer, Character>();
	
	static {
		char ch = 'a';
		for(int i = 0; i <= 25; i++) {
			Base32Table.put(i, ch);
			ch++;
		}
		
		ch = '2';
		for(int i = 26; i <= 31; i++) {
			Base32Table.put(i, ch);
			ch++;
		}
	}
	
	static public class utils {
		static String ByteTo8LenBinary(byte b) {
			String buf = Integer.toBinaryString((int) Byte.toUnsignedInt(b));
			while(buf.length() < 8) {
				buf = "0" + buf;
			}
			return buf;
		}
		
		static int BinaryToInteger(String str) {
			int power = 1;
			int res = 0;
			for(int i = str.length() - 1; i >= 0; i--) {
				if(str.charAt(i) == '1') {
					res += power;
				}
				power *= 2;
			}
			return res;
		}
	}
	
	static String basexencode(byte[] bin, int base, HashMap<Integer, Character> BaseXTable) {
		String binarydata = "";
		String res = "";
		int baselen = Integer.toBinaryString((int) base).length() - 1;
		
		for(int i = 0; i < bin.length; i++)
			binarydata += utils.ByteTo8LenBinary(bin[i]);

		while(binarydata.length()%baselen != 0)
			binarydata += "0";
		
		for(int i = 0; (i + baselen) <= binarydata.length(); i += baselen)
			res += BaseXTable.get(utils.BinaryToInteger(binarydata.substring(i, i + baselen)));
		
		return res;
	}
	
	static String i2pb64tob32(String b64) {
		b64 = b64.replace('-', '+').replace('~', '/');
		byte[] decodedb64 = Arrays.copyOfRange(Base64.getDecoder().decode(b64), 0, 391);
		
		MessageDigest sha256;
		try {
			sha256 = MessageDigest.getInstance("SHA-256");
		} catch(NoSuchAlgorithmException e) {
			System.out.println(e.getMessage());
			return null;
		}
		
		return basexencode(sha256.digest(decodedb64), 32, Base32Table) + ".b32.i2p";
	}
}
