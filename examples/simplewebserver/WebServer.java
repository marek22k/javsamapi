
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.function.Consumer;

import samapi.StreamSession;
import samapi.tcpserver;

public class WebServer {

	/* Standard host and port for the internal server, which runs on 127.0.0.1.
	 * All requests from the I2P network are forwarded on this. */
	final static String defaultHost = "127.0.0.1";
	final static Integer defaultPort = 5546;
	
	/* Lambda function, which is given to the tcpserver and is called if an exception is thrown. */
	final static Consumer<Exception> defaultExceptionHandler = (exc) -> {
		System.out.println(exc.getMessage());
		exc.printStackTrace();
	};
	
	StreamSession sess;
	tcpserver serv;
	StreamSession.StreamForwarder forwarder;
	
	public WebServer() throws IOException {
		System.out.println("Initialize stream session ...");
		sess = new StreamSession();
		sess.initialize();
	}
	
	public WebServer(String privkey, String pubkey) throws RuntimeException, IOException {
		System.out.println("Initialize stream session ...");
		sess = new StreamSession();
		sess.initialize(privkey, pubkey);
	}
	
	public String pubkey() {
		return sess.pubkey();
	}
	
	public String privkey() {
		return sess.privkey();
	}
	
	public void close() throws IOException {
		forwarder.close();
		sess.close();
		serv.close();
	}
	
	public void start() throws UnknownHostException, IOException, InterruptedException {

		System.out.println("Initialize tcp server...");
		serv = new tcpserver(WebServer.defaultPort, WebServer.defaultHost);
		
		System.out.println("Initialize stream forwarder...");
		forwarder = sess.new StreamForwarder();
		
		System.out.println("Internal server accept connections...");
		serv.accept((cli) -> {
			
			try {
				
				/* Read out and process the line sent by I2P, which contains the Base64 of the
				 * requestor as well as to_port and from_port. */
				String controlline = cli.gets();
				int tofrom = controlline.indexOf(' ');
				int toto = controlline.indexOf(' ', tofrom + 1);
				String fromport = controlline.substring(tofrom + 1, toto).trim();
				String toport = controlline.substring(toto + 1).trim();
				
				String base64 = controlline.substring(0, tofrom).trim();
				String base32 = Basing.i2pb64tob32(base64);
				String from_port = fromport.substring(fromport.indexOf('=') + 1);
				String to_port = toport.substring(toport.indexOf('=') + 1);
				
				/* Reading out the first line of the HTTP header,
				 * i.e. the HTTP protocol, path and method used. */
				String getline = cli.gets();
				
				int tob1 = getline.indexOf(' ');
				String httpmethod = getline.substring(0, tob1);
				
				int tob2 = getline.indexOf(' ', tob1 + 1);
				String httppath = getline.substring(tob1 + 1, tob2);
				
				String httppro = getline.substring(tob2 + 1);
				
				System.out.print("Request from " + base64 + " (" + base32 + "): ");
				
				String httpheader = new String();
				
				System.out.print("RH");  // Read header
				
				/* Reading out the rest of the HTTP header, which is
				 * not processed any further, but is output at the end. */
				String buf = null;
				while((buf = cli.gets()).compareTo("") != 0) {
					httpheader += buf + "\n";
				}
				
				/* Output of the data obtained. */
				System.out.println(", SA");  // Send answer
				cli.puts(httppro + " 200 OK");
				cli.puts("Server: I2P Web Server by Marek Kuethe");
				cli.puts("Connection: closed");
				cli.puts("Content-Type: text/plain");
				cli.puts("");
				cli.puts("Your destination is " + base64 + " (" + base32 + ").");
				cli.puts("You are requesting from port " + from_port + " to port " + to_port + ".");
				cli.puts("You have requested the path " + httppath + " with the HTTP protocol " + httppro + " with the HTTP method " + httpmethod + ".");
				cli.puts("");
				cli.puts("Your HTTP header looks like this:");
				cli.puts(httpheader);
				cli.close();
			} catch(Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
			
		}, WebServer.defaultExceptionHandler);
		
		System.out.println("Forward request from the i2p network to the internal server...");
		forwarder.forward(WebServer.defaultHost, WebServer.defaultPort);
		
		System.out.println("Ready!");
		serv.accept_wait();
	}
	
}
