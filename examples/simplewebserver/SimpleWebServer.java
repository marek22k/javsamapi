
/*
 * Copyright 2020 Marek Küthe
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
*/

/* This program provides a small HTTP server over the I2P network.
 * If the server is called, a page of the type text/plain with various
 * known data is output. */
/* Note: It can take a few minutes until the server can be reached
 * via the I2P network. This also depends on how much the I2P node
 * is integrated in the network. */

public class SimpleWebServer {

	/* If known, the public and private key that the
	 * server should listen to can be entered here. */
	final static String privkey = null;
	final static String pubkey  = null;
	
	static WebServer serv;
	
	public static void main(String[] args) {
		
		/* This method is used to close the server and the associated sockets
		 * if the program is terminated. */
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
            public void run() {
				try {
					serv.close();
				} catch(Exception e) {
					System.out.println(e.getMessage());
					e.printStackTrace();
				}
            }   
        });
		
		try {
			
			System.out.println("Create I2P Web Server...");
			if(privkey == null || pubkey == null)
				serv = new WebServer();
			else
				serv = new WebServer(privkey, pubkey);
			
			System.out.println("Start the server on the destination " + serv.pubkey() + " (" + Basing.i2pb64tob32(serv.pubkey()) + ") ...");
			serv.start();
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
	}

}
